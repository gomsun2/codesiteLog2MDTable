unit v.main;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, RzButton, System.ImageList, Vcl.ImgList, RzPanel, RzSplit, Vcl.ExtCtrls, Vcl.StdCtrls,
  RzStatus, System.Actions, Vcl.ActnList, Vcl.PlatformDefaultStyleActnCtrls, Vcl.ActnMan;

type
  TvMain = class(TForm)
    RzStatusBar1: TRzStatusBar;
    RzSplitter1: TRzSplitter;
    RzToolbar1: TRzToolbar;
    ImageList1: TImageList;
    ButtonExecute: TRzToolButton;
    MemoSrc: TMemo;
    MemoDst: TMemo;
    RzVersionInfoStatus1: TRzVersionInfoStatus;
    RzVersionInfo1: TRzVersionInfo;
    ButtonPaste: TRzToolButton;
    ActionManager1: TActionManager;
    ActionPaste: TAction;
    ActionExecute: TAction;
    ButtonCopy: TRzToolButton;
    ActionCopy: TAction;
    ActionSelectAll: TAction;
    ButtonInsertLineNumber: TRzToolButton;
    ButtonProperties: TRzToolButton;
    procedure ActionPasteExecute(Sender: TObject);
    procedure ActionExecuteExecute(Sender: TObject);
    procedure ActionSelectAllExecute(Sender: TObject);
    procedure ActionCopyExecute(Sender: TObject);
  private
  public
  end;

var
  vMain: TvMain;

implementation

{$R *.dfm}

uses
  Clipbrd, System.NetEncoding, System.StrUtils, System.Math
  ;

procedure TvMain.ActionCopyExecute(Sender: TObject);
begin
  MemoDst.SelectAll;
  Application.ProcessMessages;
  Clipboard.AsText := Trim(MemoDst.Text);
end;

procedure TvMain.ActionExecuteExecute(Sender: TObject);
var
  LHeader, LBuf: TStringWriter;
  isp, LHeaderCnt, i, c: Integer;
  LCols, LNames, LValues: TArray<String>;
  LSp, LStyle: String;
begin
  LHeaderCnt := -1;
  LBuf := TStringWriter.Create;
  for i := 0 to MemoSrc.Lines.Count -1 do
  begin
    if LHeaderCnt = -1 then
      LHeaderCnt := MemoSrc.Lines[i].CountChar(#9) +1 + IfThen(ButtonInsertLineNumber.Down, 1);
    LCols := MemoSrc.Lines[i].Split([#9]);
    LSp := '';
    if Length(LCols) > 0 then
    begin
      LStyle := IfThen(LCols[0] = 'Error', '***', IfThen(LCols[0].Trim <> 'Info', '**'));
      for iSp := 0 to LCols[0].CountChar(' ') -1 do
        LSp := LSp +'&nbsp;';
    end;

    if ButtonInsertLineNumber.Down then
    begin
      LBuf.Write('|');
      LBuf.Write(LStyle);
      LBuf.Write((i +1).ToString);
      LBuf.Write(LStyle);
    end;
    for c := 0 to Length(LCols) -1 do
    begin
      LBuf.Write('|');
      LBuf.Write(LStyle);
      LBuf.Write(IfThen(c in [0, 1], LSp) + TNetEncoding.HTML.Encode(LCols[c].Trim));
      LBuf.Write(LStyle);
    end;
    LBuf.Write('|');
    LBuf.WriteLine;
  end;

  SetLength(LNames, LHeaderCnt);
  if ButtonInsertLineNumber.Down then
    LValues := ['No'];
  LValues := LValues + ['', 'Message', 'Category', 'TimeStamp'];
  for i := 0 to Length(LNames) -1 do
    LNames[i] := 'Col' + (i+1).toString;
  LHeader := TStringWriter.Create;
  try
    if ButtonProperties.Down then
      InputQuery('Input header columns', LNames, LValues);

    LHeader.Write('|');
    for i := 0 to LHeaderCnt -1 do
    begin
      LHeader.Write(LValues[i]);
      LHeader.Write('|');
    end;
    LHeader.WriteLine;

    LHeader.Write('|');
    for i := 0 to LHeaderCnt -1 do
    begin
      LHeader.Write(IfThen(ButtonInsertLineNumber.Down and (i = 0), '---:', '---'));
      LHeader.Write('|');
    end;
    LHeader.WriteLine;

    MemoDst.Lines.Append(LHeader.ToString.Trim);
    MemoDst.Lines.Append(LBuf.ToString);
  finally
    FreeAndNil(LHeader)
  end;
  LBuf.Free;
end;

procedure TvMain.ActionPasteExecute(Sender: TObject);
begin
  MemoSrc.Text := Clipboard.AsText;
end;

procedure TvMain.ActionSelectAllExecute(Sender: TObject);
begin
  MemoDst.SelectAll;
end;

end.
